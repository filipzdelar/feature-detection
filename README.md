FEATURE DETECTION PACKAGE
======================

To get all required libraries needed for this project, run next command:

    pip install -r requirements.txt




STRUCTUR OF THE PROJECT
=======================

feature-detection/
|-- methods/
|   |-- black_white.py
|   |-- blur.py
|   |-- canny.py
|   |-- fast_corner_detetion.py
|   |-- sobel.py
|
|-- output/
|   |-- image_black_white.png
|   |-- image_blur.png
|   |-- image_canny.png
|   |-- image_corner.png
|   |-- image_normal.png
|   |-- image_sobel.png
|     
|-- pic/
|   |-- flower.py
|
|-- main.py
|-- README.md

/methods/  - black-white , blur, canny, fast corner detetion, sobel
/output/   - default directory for the output images with examples of the flower
/pic/      - usual directory for the input images




WAYS TO USE THE PROJECT
=======================

python main.py --path ./pic/flower3.png

possible arguments:

'--path', 
        default = '.',
        help = 'sum the integers (default: find the max)', 
        required=True

'--bw', 
        default = "black_white_yuv",
        help = 'sum the integers (default: find the max)')
    
'--size_of_gauss_kernel', 
        default = 3,
        help = 'size of kernel = size_of_gauss_kernel * 2 +1')

'--sigma', 
        default = 0.84089642,
        help = 'coeficient of fuzziness')

'--offset', 
        default = 0.5,
        help = 'The amount of sharpness to define the corner')
    
'--offset_inverese', 
        default = 0.95,
        help = 'The amount of sharpness to define the inverese corner')

'--lower_pix_inverese', 
        default = 4,
        help = 'minimum array of pixeles to define the inverese corner')
    
'--upper_inverese', 
        default = 6,
        help = 'maximum array of pixeles to define the inverese corner')
    
'--lower_pix', 
        default = 3,
        help = 'minimum array of pixeles to define the corner')
    
'--upper_pix', 
        default = 7,
        help = 'maxiumum array of pixeles to define the corner')





ABOUT THE CODE
==============

This is project of the faculty of tehnical science.
The main work have been split into two part and six methodes, each with three methode. We were in general interested in this topic and we would like to research more application of this field, expectialy.

Methods:
-   Transformacije u crno-bele slike    [Milovanociv Uros]
-   Blurovanje                          [Zdelar Filip]
-   Sobel operator                      [Milovanociv Uros]
-   Canny edge detector                 [Milovanociv Uros]
-   Connecting the dots                 [Zdelar Filip]
-   FAST corner detection               [Zdelar Filip]

The proposal for the project is at next link:
    https://docs.google.com/document/d/1Ed0C_tWLxSpYkd_W7vKxgzCrutfMZze5f5JVc-x8PW0/edit#

Presentation is at followed link:
    https://docs.google.com/presentation/d/1ODoQJHlRYcCoNLL0ztcCW9gX59loicpgHJMI3EHadHw/edit#slide=id.g6f15ec071a_0_4




REFERENCES
==========

http://www.tannerhelland.com/3643/grayscale-image-algorithm-vb6/ - Grey transformation
https://en.wikipedia.org/wiki/Gaussian_blur - Blurring
https://www.youtube.com/watch?v=C_zFhWdM4ic - Gaussian blur
https://medium.com/@nikatsanka/comparing-edge-detection-methods-638a2919476e  - Prewitt and Sobel operator
https://www.youtube.com/watch?v=uihBwtPIBxM - Sobel operator
https://docs.opencv.org/2.4/doc/tutorials/imgproc/imgtrans/canny_detector/canny_detector.html - Canny edge detector
https://www.youtube.com/watch?v=sRFM5IEqR2w - Canny edge detector
https://www.edwardrosten.com/work/fast.html - FAST corner detector
https://medium.com/software-incubator/introduction-to-fast-features-from-accelerated-segment-test-4ed33dde6d65 - FAST corner detector         
https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4099172/ 





CONTACT
=======

Please send bug reports, suggestions, and other feedback to

  mail: filipzdelar@gmail.com, urkem98@gmail.com
