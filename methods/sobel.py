from math import sqrt,atan2,pi
import numpy as np

def sobel(pic_array,new_pic_array):
    for x in range(1, len(pic_array) - 1):
        for y in  range(1, len(pic_array[x]) - 1):
            derivative_y = 0
            derivative_x = 0
            
            derivative_y += pic_array[x + 1][y + 1][0]
            derivative_y += 2*pic_array[x + 1][y][0]
            derivative_y += pic_array[x + 1][y - 1][0]
            derivative_y += -pic_array[x - 1][y + 1][0]
            derivative_y += -2*pic_array[x - 1][y][0]
            derivative_y += -pic_array[x - 1][y - 1][0]


            derivative_x += pic_array[x + 1][y + 1][0]
            derivative_x += 2*pic_array[x][y + 1][0]
            derivative_x += pic_array[x - 1][y + 1][0]
            derivative_x += -pic_array[x + 1][y - 1][0]
            derivative_x += -2*pic_array[x][y - 1][0]
            derivative_x += -pic_array[x - 1][y - 1][0]

            derivative_y = derivative_y/3
            derivative_x = derivative_x/3
            
            angle = atan2(derivative_y,derivative_x) * 180 / np.pi
            if(angle < 0):
                angle += 180
            derivative = sqrt(derivative_y**2 + derivative_x**2)
            
            new_pic_array[x][y] = [derivative, angle, angle, pic_array[x][y][3]]

