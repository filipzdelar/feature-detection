import numpy as np

def generate_gauss_m(offset, sigma):
    length = offset*2+1
    GaussM =  np.zeros([length, length])
    print(sigma)
    for x in range(length):
        for y in range(length):
            GaussM[x,y] = (1/(2 * np.pi * (sigma**2))) *  np.exp(-((offset - x)**2 + ((offset - y)**2))/(2*(sigma**2)))
    np.set_printoptions(suppress = True)
    print(GaussM)
    return GaussM



def blur(pic_array, offset, sigma):
    GaussM = generate_gauss_m(offset, sigma)
    num_of_pixels = (offset*2+1)**2
    dev = np.sum(GaussM)
    
    for x in range(offset, len(pic_array) -offset):
        for y in  range(offset, len(pic_array[x]) -offset):
            avg = 0
            for x_offet in range(-offset,offset+1):
                for y_offet in range(-offset,offset+1):
                    avg += pic_array[x + x_offet][y + y_offet][0] * GaussM[x_offet+offset][y_offet+offset]
            avg /=  dev 
            pic_array[x][y] = [avg, avg, avg, pic_array[x][y][3]]
    