def black_white_color_avrage(pic_array):
    for x in pic_array:
        for y in x:
            avgRGB = (y[0] + y[1] + y[2]) /3
            y[:-1] = [avgRGB, avgRGB, avgRGB]

def black_white_yuv(pic_array):
    for x in pic_array:
        for y in x:
            avgRGB = 0.2989 * y[0] + 0.5870 * y[1] + 0.1140 * y[2]
            y[:-1] = [avgRGB, avgRGB, avgRGB]