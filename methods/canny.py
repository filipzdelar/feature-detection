import numpy as np

def canny(pic_array,new_pic_array):
    for x in range(1, len(pic_array)-1):
        for y in  range(1, len(pic_array[x])-1):
            q = 255
            r = 255
            #angle 0
            if (0 <= pic_array[x][y][1] < 22.5) or (157.5 <= pic_array[x][y][1] <= 180):
                q = pic_array[x][y+1][0]
                r = pic_array[x][y-1][0]
            #angle 45
            elif (22.5 <= pic_array[x][y][1] < 67.5):
                q = pic_array[x+1][y-1][0]
                r = pic_array[x-1][y+1][0]
            #angle 90
            elif (67.5 <= pic_array[x][y][1] < 112.5):
                q = pic_array[x+1][y][0]
                r = pic_array[x-1][y][0]
            #angle 135
            elif (112.5 <= pic_array[x][y][1] < 157.5):
                q = pic_array[x-1][y-1][0]
                r = pic_array[x+1][y+1][0]

            if (pic_array[x][y][0] >= q) and (pic_array[x][y][0] >= r):
                new_pic_array[x][y] = [pic_array[x][y][0],pic_array[x][y][0],pic_array[x][y][0],1.0]
            else:
                new_pic_array[x][y] = [0,0,0,1.0]




def double_treshold(pic_array, new_pic_array, highThreshold, lowThreshold):
    img = pic_array[:,:,0]
    highThreshold = img.max() * highThreshold
    lowThreshold = highThreshold * lowThreshold
    
    M, N = img.shape
    res = np.zeros((M, N), dtype = np.float32)
    
    weak = np.float32(0.333)
    strong = np.float32(1.0)
    
    strong_i, strong_j = np.where(img >= highThreshold)
    zeros_i, zeros_j = np.where(img < lowThreshold)
    
    weak_i, weak_j = np.where((img <= highThreshold) & (img >= lowThreshold))
    
    res[weak_i, weak_j] = weak
    res[strong_i, strong_j] = strong

    for x in range(1, len(pic_array) - 1):
        for y in  range(1, len(pic_array[x]) - 1):
            new_pic_array[x, y] = [res[x,y], res[x,y], res[x,y], np.float32(1.0)]
    

def hysteresis(pic_array, weak, strong):
    for i in range(1, len(pic_array) - 1):
        for j in  range(1, len(pic_array[i]) - 1):
            if (pic_array[i, j][0] == weak):
                if ((pic_array[i + 1, j - 1][0] == strong) or (pic_array[i + 1, j][0] == strong) or (pic_array[i + 1, j + 1][0] == strong)
                    or (pic_array[i, j - 1][0] == strong) or (pic_array[i, j + 1][0] == strong)
                    or (pic_array[i - 1, j - 1][0] == strong) or (pic_array[i - 1, j][0] == strong) or (pic_array[i - 1, j + 1][0] == strong)):
                    pic_array[i, j] = [strong,strong,strong,1.0]
                else:
                    pic_array[i, j] = [0, 0, 0, 1.0]