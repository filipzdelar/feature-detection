import numpy as np

def connect(inputt):
    output = inputt
    return output

# 16 + 4
radius = [[0,3], 
          [1,3],
          [2,2],
          [3,1],
          [3,0],
          [3,-1],
          [2,-2],
          [1,-3],
          [0,-3],
          [-1,-3],
          [-2,-2],
          [-3,-1], 
          [-3,0],
          [-3,1],
          [-2,2],
          [-1,3],
          [0,3], 
          [1,3],
          [2,2],
          [3,1]]

coreners = []
coreners_inverse = []

def fast_corner(input_pic, offset, offset_inverese, lower_pix_inverese, upper_inverese, lower_pix, upper_pix):
    from_pix = 0
    output = input_pic

    for x in range(4, len(input_pic)-4):
        for y in  range(4, len(input_pic[x])-4):
            
            brighter = -1
            darker = -1
            is_corner = 0   

            for z in range(1, len(radius)):
                if(darker == -1 and (input_pic[x][y][from_pix] < input_pic[x + radius[z][0]][y + radius[z][1]][from_pix] + offset_inverese)):
                    darker = 0
                
                if(darker >= 0 and darker <= lower_pix_inverese):
                    if(input_pic[x][y][from_pix] > input_pic[x + radius[z][0]][y + radius[z][1]][from_pix] + offset_inverese):
                        darker += 1 
                    else:
                        darker = 0
                else:
                    if(darker > lower_pix_inverese and darker < upper_inverese):
                        if(input_pic[x][y][from_pix] < input_pic[x + radius[z][0]][y + radius[z][1]][from_pix] + offset_inverese):
                            is_corner = 1
                            break
                        else:
                            darker += 1
                    else:
                        if(darker == upper_inverese):
                            darker = -1
                
            if(is_corner):
                coreners_inverse.append([x,y])
            
            brighter = -1
            darker = -1
            is_corner = 0 

            for z in range(1, len(radius)):
                if(brighter == -1 and (input_pic[x][y][from_pix] > input_pic[x + radius[z][0]][y + radius[z][1]][from_pix] + offset)):
                    brighter = 0
                
                if(brighter >= 0 and brighter <= lower_pix):
                    if(input_pic[x][y][from_pix] < input_pic[x + radius[z][0]][y + radius[z][1]][from_pix] + offset):
                        brighter += 1 
                    else:
                        brighter = 0
                else:
                    if(brighter > lower_pix and brighter < upper_pix):
                        if(input_pic[x][y][from_pix] > input_pic[x + radius[z][0]][y + radius[z][1]][from_pix] + offset):
                            is_corner = 1
                            break
                        else:
                            brighter += 1
                    else:
                        if(brighter == upper_pix):
                            brighter = -1
                
            if(is_corner):
                coreners.append([x,y])


    for corner in range(1, len(coreners_inverse)):
        for f in range(1, len(radius)):
            output[coreners_inverse[corner][0] + radius[f][0], coreners_inverse[corner][1] + radius[f][1]] = [0, 0, 0.9, 1] 


    for corner in range(1, len(coreners)):
        for f in range(1, len(radius)):
            output[coreners[corner][0] + radius[f][0], coreners[corner][1] + radius[f][1]] = [0.9, 0, 0, 1] 

    
    return output
