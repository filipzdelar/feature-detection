import argparse
import matplotlib.pyplot as plt
import numpy as np
from methods.black_white import *
from methods.blur import *
from methods.sobel import *
from methods.canny import *
from methods.fast_corner_detection import *
import cv2

save_num = 0
save_file = ['_normal', '_black_white', '_blur', '_canny', '_corner']


def outputImage(pic_array, isplotable, issavable):
    if isplotable == True:
        global save_num
        global save_file
        plt.imshow(pic_array)
        plt.savefig('./output/image'+save_file[save_num]+'.png')
        plt.show()
        save_num += 1

if __name__ == "__main__":
    # python main.py --path=./pic/town.png 
    parser = argparse.ArgumentParser()

    parser.add_argument('--path', default = '.',
                    help='sum the integers (default: find the max)', required=True)
    
    # Arguments for the methode Black White
    parser.add_argument('--bw', default = "black_white_yuv",
                    help='sum the integers (default: find the max)')
    
    #  Arguments for the methode Blur
    parser.add_argument('--size_of_gauss_kernel', default = 3,
                    help='size of kernel = size_of_gauss_kernel * 2 +1')
    parser.add_argument('--sigma', default = 0.84089642,
                    help='coeficient of fuzziness')

    #  Arguments for the methode fast corner detection
    parser.add_argument('--offset', default = 0.5,
                    help='The amount of sharpness to define the corner')
    parser.add_argument('--offset_inverese', default = 0.95,
                    help='The amount of sharpness to define the inverese corner')
    parser.add_argument('--lower_pix_inverese', default = 4,
                    help='minimum array of pixeles to define the inverese corner')
    parser.add_argument('--upper_inverese', default = 6,
                    help='maximum array of pixeles to define the inverese corner')
    parser.add_argument('--lower_pix', default = 3,
                    help='minimum array of pixeles to define the corner')
    parser.add_argument('--upper_pix', default = 7,
                    help='maxiumum array of pixeles to define the corner')

    #  Arguments for the methode output
    parser.add_argument('--plot', default = True,
                    help='plot image per each step')
    parser.add_argument('--save', default = True,
                    help='save the images')
    
    args = parser.parse_args()

    pic = plt.imread(args.path)
    pic_array = np.array(pic)
    outputImage(pic_array, args.plot, args.save)
    
    if args.bw == 'black_white_color_avrage':
        black_white_color_avrage(pic_array)
    else:
        black_white_yuv(pic_array)
    outputImage(pic_array, args.plot, args.save)
    
    pic_corner = np.copy(pic_array)
    
    blur(pic_array, args.size_of_gauss_kernel , args.sigma)
    outputImage(pic_array, args.plot, args.save)
    sobel_output = np.zeros(pic_array.shape)
    canny_output = np.zeros(pic_array.shape)
    double_output = np.zeros(pic_array.shape)

    sobel(pic_array,sobel_output)
    canny(sobel_output,canny_output)
    double_treshold(canny_output, double_output, 0.4, 0.2)
    hysteresis(double_output, np.float32(0.333), np.float32(1.0))
    outputImage(double_output, args.plot, args.save)
    
    final_output = fast_corner(pic_corner,  offset = args.offset,
                                            offset_inverese = args.offset_inverese, 
                                            lower_pix_inverese = args.lower_pix_inverese, 
                                            upper_inverese = args.upper_inverese, 
                                            lower_pix = args.lower_pix, 
                                            upper_pix = args.upper_pix)
    
    outputImage(final_output, args.plot, args.save)


